# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Angular components for FSA style guide components
* 1.0.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Install Node on your machine.
* Install angular-cli in your machine using 'npm install -g @angular/cli'
* clone this repository using git clone [url]
* Go to the project folder and do 'ng new fsa-style-angularComponents' to create an angular project.
* Npm install fsa_style repo in the same folder using npm install --save fsa-style
* Add 'fsa-style.min.css' file path in styles in the .angular-cli.json file to apply fsa-styles to the angular components.  


* Development instructions
* Create your branch in git using 'git checkout -b [new branch name]' (e.g. git checkout -b feature/WES1)and start development.
* Run 'ng serve' in the project folder and open localhost to see our changes visually. 
* Once done with the development, add files to staging area using 'git add [file name]'. 
* To add all files at a time do git add --all
* commit files using git commit -m "description of commit"
* push your branch using git push -u origin [yourbranchname]

* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact