import { Directive, HostListener, ElementRef, Renderer2, HostBinding  } from '@angular/core';

@Directive({
  selector: '[appDropdown]'
})
export class DropdownDirective {
  @HostBinding('class.open') isOpen = false;
  @HostListener('click') toggleOnOpen() {
   this.isOpen = !this.isOpen;
  }

  // constructor(private eventRef: ElementRef, private renderer: Renderer2) { }

}
