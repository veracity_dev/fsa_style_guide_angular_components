import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ButtongroupsComponent } from './buttongroups.component';

describe('ButtongroupsComponent', () => {
  let component: ButtongroupsComponent;
  let fixture: ComponentFixture<ButtongroupsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ButtongroupsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtongroupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
