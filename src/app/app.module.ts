import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { DesignStylesComponent } from './design-styles/design-styles.component';
import { ButtongroupsComponent } from './buttongroups/buttongroups.component';
import { TextAreaComponent } from './text-area/text-area.component';
import { SelectComponent } from './select/select.component';
import { LabelsComponent } from './labels/labels.component';
import { GridComponent } from './grid/grid.component';
import { DropdownDirective } from './dropdown.directive';


@NgModule({
  declarations: [
    AppComponent,
    DesignStylesComponent,
    ButtongroupsComponent,
    TextAreaComponent,
    SelectComponent,
    LabelsComponent,
    GridComponent,
    DropdownDirective
  
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
